provider "google" {
  project = var.project-id
  region  = "europe-west1"
  zone    = "europe-west1-b"
}

terraform {
  backend "http" {
  }
  required_providers {
    google = {
      source  = "google"
      version = "~> 4.81"
    }
  }
}

resource "google_compute_network" "vpc_network" {
  name = "vpc-test-emmanuel-barthelemy"
}